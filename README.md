## ENV

Before Cypress is full functional first follow below steps:

1. In the Cypress folder (highest level) create a file with name .env (this file will not be committed to Gitlab)
2. Enter the credentials according to the format in the .env.example file
3. Overwrite username and password including < and > with the values you created for your account
