const { defineConfig } = require("cypress");
require('dotenv').config()

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
    },
    baseUrl: "https://practicesoftwaretesting.com",
    env:{
      username: process.env.ACCOUNT_USERNAME,
      password: process.env.ACCOUNT_PASSWORD
    }
  },
});
