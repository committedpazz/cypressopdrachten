import "./cmSession.js";

Cypress.Commands.add("login", (email, password) => {
  cy.get('[data-test="email"]').type(email);
  cy.get('[data-test="password"]').type(password);
  cy.get('[data-test="login-submit"]').click();
  cy.get('[data-test="page-title"]').should("include.text", "My account");
});

Cypress.Commands.add("register", ({ firstName, lastName, email, password }) => {
  cy.request({
    method: "POST",
    url: "https://api.practicesoftwaretesting.com/users/register",
    body: {
      first_name: firstName,
      last_name: lastName,
      dob: "1999-02-01",
      address: "Orteliuslaan 1000",
      city: "Utrecht",
      state: "Utrecht",
      country: "NL",
      postcode: "3528BD",
      phone: "0612345678",
      email: email,
      password: password,
    },
  }).then((responsePOST) => {
    expect(responsePOST.status).to.eq(201);
  });
});

Cypress.Commands.add("loginViaAPI", (email, password) => {
  cy.request({
    method: "POST",
    url: "https://api.practicesoftwaretesting.com/users/login",
    body: {
      email: email,
      password: password,
    },
  }).then((responsePOST) => {
    expect(responsePOST.status).to.eq(200);
  });
});
