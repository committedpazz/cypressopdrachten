Cypress.Commands.add("loginSession", (email, password) => {
  cy.session(
    [email, password],
    () => {
      cy.visit("/#/auth/login");
      cy.get('[data-test="email"]').type(email);
      cy.get('[data-test="password"]').type(password);
      cy.get('[data-test="login-submit"]').click();
      cy.get('[data-test="page-title"]').should("include.text", "My account");
    },
    {
      validate() {
        cy.visit("/#/account");
      },
    }
  );
  cy.visit("/#/account");
});

Cypress.Commands.add("loginSessionAPI", (email, password) => {
  cy.session([email, password], () => {
    cy.loginViaAPI(email, password);
  }, {
    validate() {
      cy.visit("/#/account");
    },
  });
  cy.visit("/#/account");
});
