// Warning: only run this test once with the same values

describe('CreateAccount', () => {
    it('Create an account to test with', () => {
        cy.register({
            firstName: 'John',
            lastName: 'Doe',
            email: 'johndoe@mail.nl',
            password: 'ValoriRules456!'
        })
    })
    it.skip('Create an account to test with - using dotenv', () => {
        cy.register({
            firstName: 'Pietje',
            lastName: 'Puk',
            email: Cypress.env('username'),
            password: Cypress.env('password')
        })
    })
});
