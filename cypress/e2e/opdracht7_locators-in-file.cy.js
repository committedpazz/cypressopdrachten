import selectors from "./selectors";

describe('locators in file', () => {
    beforeEach(() => {
        cy.visit('/#/auth/register');
    });
    it('can fill in locator values from file', () => {
        // selectors in selector.js file
        cy.get(selectors.register.firstName).type('John');
        cy.get(selectors.register.lastName).type('Doe');
        cy.get(selectors.register.email).type('tester@test.nl');        
    });
});