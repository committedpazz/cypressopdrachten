describe('Categories', () => {
    beforeEach(() => {
        cy.visit('/');
    });
    it('has 5 categories', () => {
        cy.get('[data-test="nav-categories"]+ul li a[class="dropdown-item"]').should('have.length', 5);
    });
});