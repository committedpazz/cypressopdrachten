describe('experiment with skip and only', () => {
    it('will be runned if no .only testcase is there', () => {
        cy.log('normal situation')
    });
    it.skip('will be skipped', () => {
        cy.log('skipped situation')
    });
    it.only('will be runned', () => {
        cy.log('only situation')
    });
});