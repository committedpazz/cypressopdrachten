describe("LoginPage", () => {
  beforeEach(() => {
    cy.visit("/#/auth/login");
  });
  it("Login successfull", () => {
    cy.login("pietje.puk@valori.nl", "Kalender789!");
  });

  it("Login successfull via API", () => {
    cy.loginViaAPI("pietje.puk@valori.nl", "Kalender789!");
  });
});
