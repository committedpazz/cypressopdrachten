import Headers from "./Page Objects/header-component";
import LoginPage from "./Page Objects/login";
import AccountPage from "./Page Objects/account";
import HomePage from "./Page Objects/homePage";

describe("Account options", () => {
  const header = new Headers();
  const loginPage = new LoginPage();
  const accountPage = new AccountPage();
  const homePage = new HomePage();

  beforeEach(() => {
    homePage.visit();
    header.navigateToSignIn();
    loginPage.loginAccount(Cypress.env("username"), Cypress.env("password"));
  });
  it("can open profile", () => {
    accountPage.navigateToProfile();
  });
});
