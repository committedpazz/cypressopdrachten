import { faker } from "@faker-js/faker";

describe('Use faker for data', () => {
    beforeEach(() => {
        cy.visit('/#/auth/register');
    });
    it('Login successfull - using dotenv', () => {
        const firstName = faker.person.firstName();
        cy.get('[data-test="first-name"]').type(firstName);
    });
});