describe("Prizes", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("cy.wrap solution - prize is the same everywhere on the website", () => {
    cy.get('[data-test*="product"]')
      .contains("Combination Pliers")
      .parent()
      .parent()
      .find('[data-test="product-price"]')
      .then(($price) => {
        cy.wrap($price.text()).as("productPrice");
      });
    cy.get('[data-test*="product"]').contains("Combination Pliers").click();
    cy.get('[data-test="add-to-cart"]').click();
    cy.get('[aria-label="Product added to shopping cart."]').should(
      "be.visible"
    );
    cy.get('[data-test="cart-quantity"]').should("have.text", "1");
    cy.get('[data-test="cart-quantity"]').click();
    cy.contains("Combination Pliers")
      .parent("td")
      .parent("tr")
      .within(() => {
        cy.get("td span").eq(1).as("priceCart");
        cy.get("@productPrice").then((productPrice) => {
          cy.get("@priceCart").should("have.text", productPrice);
        });
      });
  });

  it("js-solution - prize is the same everywhere on the website", () => {
    let productPrice;
    cy.get('[data-test*="product"]')
      .contains("Combination Pliers")
      .parent()
      .parent()
      .find('[data-test="product-price"]')
      .then(($price) => {
        productPrice = $price.text();
      });
    cy.get('[data-test*="product"]').contains("Combination Pliers").click();
    cy.get('[data-test="add-to-cart"]').click();
    cy.get('[aria-label="Product added to shopping cart."]').should(
      "be.visible"
    );
    cy.get('[data-test="cart-quantity"]').should("have.text", "1");
    cy.get('[data-test="cart-quantity"]').click();
    cy.contains("Combination Pliers")
      .parent("td")
      .parent("tr")
      .within(() => {
        cy.get("td span").eq(1).should("have.text", productPrice);
      });
  });
});
