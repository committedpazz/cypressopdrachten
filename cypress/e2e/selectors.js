export default {
    // Selectors
    register: {
        firstName: '[data-test="first-name"]',
        lastName: '[data-test="last-name"]',
        email: '[data-test="email"]',
    },
    categories: {
        handTools: '[data-test="nav-hand-tools"]',
        powerTools: '[data-test="nav-power-tools"]'
    }
};