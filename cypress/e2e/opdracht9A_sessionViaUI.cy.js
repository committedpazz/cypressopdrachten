describe("LoginPage", () => {
  beforeEach(() => {
    // making use of dotenv. See Bonusopdracht A
    cy.loginSession(Cypress.env("username"), Cypress.env("password"));
  });

  it("Do this without extra login", () => {
    cy.get('[data-test="nav-profile"]').click();
    cy.contains("Profile");
  });
  it("Do that without extra login", () => {
    cy.get('[data-test="nav-invoices"]').click();
    cy.contains("Invoices");
  });
});
