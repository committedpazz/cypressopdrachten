class Header {
  navigateToHome() {
    cy.get('[data-test="nav-home"]').click();
  }
  navigateToContact() {
    cy.get('[data-test="nav-contact"]').click();
  }
  navigateToSignIn() {
    cy.get('[data-test="nav-sign-in"]').click();
  }
}

export default Header;
