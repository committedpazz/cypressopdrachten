class AccountPage {
  navigateToProfile() {
    cy.get('[data-test="nav-profile"]').click();
  }
}

export default AccountPage;
